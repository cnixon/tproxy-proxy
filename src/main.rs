// Rip off https://github.com/danisla/kubernetes-tproxy to add proxy redirection
// Implement our own operator using https://github.com/clux/kube-rs
// Use low level hyper API to extract IP from the incoming TCP and use that for the outgoing connection.
// See if that connection can be reused? :shrug:

// External:
// https://github.com/ahupowerdns/tproxydoc/blob/master/tproxy.md
// https://www.linuxquestions.org/questions/linux-networking-3/routing-locally-generated-packets-825040/

#![deny(warnings)]

use std::process;

use futures_util::future::FutureExt;
use futures_util::try_future::TryFutureExt;

use http;

use hyper::server::conn::Http;
use hyper::service::service_fn;
use hyper::service::Service;
use hyper::{Body, Request};

use hyper::client::conn::Builder;
use hyper::client::connect::HttpConnector;
use hyper::client::service::Connect;

use nix::sys::socket::{self, sockopt::IpTransparent, sockopt::ReuseAddr};
use std::os::unix::io::AsRawFd;
use tokio::net::TcpListener;
use tokio::runtime::Runtime;
use tokio_net::driver::Handle;

use privdrop;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Create the runtime
    let rt = Runtime::new()?;

    // Bind to localhost and setsockopts
    let listener = std::net::TcpListener::bind("127.0.0.1:12345")
        .expect("Failed to bind Socket");
    socket::setsockopt(listener.as_raw_fd(), ReuseAddr, &true)
        .expect("Failed to set ReuseAddr sockopt");
    socket::setsockopt(listener.as_raw_fd(), IpTransparent, &true)
        .expect("Failed to set IpTransparent sockopt");

    privdrop::PrivDrop::default()
        .user("proxy")
        .group("proxy")
        .apply()
        .unwrap_or_else(|e| panic!("Failed to drop privileges: {}", e));

    // Spawn the root task
    rt.block_on(async {
        pretty_env_logger::init();

        // Convert std::net::TcpListener to tokio
        let mut listener = TcpListener::from_std(listener, &Handle::default())
            .expect("Tokio TcpListener creation failed");
        println!("{}", process::id());

        loop {
            match listener.accept().await {
                Ok((stream, _addr)) => {
                    let local_addr = stream.peer_addr().map(|s| s.ip()).ok();
                    let service = service_fn(move |req: Request<Body>| {
                        async move {
                            // Create http_connector
                            // TODO: create our own resolver that uses stream's dst address
                            let mut http_connector = HttpConnector::new();
                            // Bind before connect, use tproxy'd local address
                            println!(
                                "Binding local_address as {:?}",
                                local_addr
                            );
                            http_connector.set_local_address(local_addr);
                            http_connector.set_reuse_address(true);
                            let mut mk_svc =
                                Connect::new(http_connector, Builder::new());
                            println!("REQ: {:?}", req);
                            println!("HOST: {:?}", req.headers()["host"]);

                            // This is broken TODO: Fix
                            let uri = http::uri::Builder::new()
                                .scheme(
                                    req.headers()
                                        .get("scheme")
                                        .map(|s| s.as_bytes())
                                        .unwrap_or(b"http"),
                                )
                                .authority(req.headers()["host"].as_bytes())
                                .path_and_query(
                                    req.uri()
                                        .path_and_query()
                                        .map(http::uri::PathAndQuery::as_str)
                                        .unwrap_or("/"),
                                )
                                .build()
                                .unwrap();
                            println!("TARGET_URI: {}", uri);
                            let mut svc = mk_svc.call(uri).await.unwrap();

                            let rsp = svc.call(req).await;
                            println!("RESP: {:?}", rsp);
                            rsp
                        }
                    });
                    let http = Http::new();
                    let conn = http.serve_connection(stream, service);

                    let fut = conn.map_err(|e| {
                        eprintln!("server connection error: {}", e);
                    });
                    let fut = fut.map(|_| ());
                    hyper::rt::spawn(fut);
                }
                Err(e) => println!("{:?}", e),
            }
        }
    });
    Ok(())
}
