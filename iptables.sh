#!/bin/bash

# Mangle packets destined for qub.ac.uk so they get sent to the right port on localhost and marked so they get routed correctly
iptables -t mangle -I PREROUTING -d 143.117.1.0/24 -p tcp -j TPROXY --tproxy-mark 0x1/0x1 --on-port=12345 --on-ip=127.0.0.1
iptables -t mangle -I OUTPUT -d 143.117.1.0/24 -p tcp -j MARK -m owner ! --uid-owner proxy --set-mark 0x1/0x1

ip rule add fwmark 1 lookup 100
ip route add local 0.0.0.0/0 dev lo table 100
